import { Component } from '@angular/core';

@Component({
  selector: 'app-podcasts',
  templateUrl: './podcasts/views/podcasts.component.html'
})
export class AppComponent {
  title = 'Creed Interactive Test';
}
