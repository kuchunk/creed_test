import { Component, OnInit } from '@angular/core';
import { PodcastsService } from '../services/podcasts.service';
import { IHttpResponse } from '../interfaces/iHttpResponse';
import { PodcastModel } from '../models/podcastModel';
import { IPodcast } from '../interfaces/podcasts';

@Component({
  selector: 'app-podcasts',
  templateUrl: '../views/podcasts.component.html'
})
export class PodcastsComponent implements OnInit {
  constructor(private podcastsService: PodcastsService, private _podcastModel: PodcastModel) { }

  podcasts = [] as Array<IPodcast>;
  apiResp: any;
  iHttpResponse: IHttpResponse;

  ngOnInit() {
    this.getPodcasts();

    //alert('Font loaded? ' + document.fonts.check('1em "Arial"'));
  }

  getPodcasts() {
      this.podcastsService.getTopWebDesigns().subscribe(res => {
        this.podcasts = this._podcastModel.filterPodcasts(res);
      });
  }

  toggleMenu() {
    alert('Menu coming soon?');
  }
}
