import { Injectable } from '@angular/core';
import { IPodcast } from '../interfaces/podcasts';

@Injectable()
export class PodcastModel {

    filterPodcasts(data: any): any {
        const respArray = [] as Array<IPodcast>;
        const topFive = data.podcasts.slice(0, 5);

        (topFive).forEach((row: any) => {
            const aPodcast: IPodcast = {
                title: row.title,
                publisher: row.publisher,
                thumbnail: row.thumbnail,
                total_episodes: row.total_episodes,
                description: this.limitChars(row.description, 200),
                itunes_id: row.itunes_id,
                rss: row.rss,
                website: row.website
            };

            respArray.push(aPodcast);
        });

        return respArray;
    }

    limitChars(txt: string, limit: number): string {
        return txt.length > limit ? txt.substring(0, limit) + '...' : txt;
    }
}
