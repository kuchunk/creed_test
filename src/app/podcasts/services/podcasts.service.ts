import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PodcastsService {

    basePath = 'https://listen-api.listennotes.com/api/v2/';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'X-ListenAPI-Key': 'a0edc2e1aae7486db43acf4bdaeb206a'
        })
    };

    constructor(private http: HttpClient) { }

    getTopWebDesigns(): Observable<any> {
        return this.http.get(this.basePath + 'best_podcasts?genre_id=140&page=1&region=us&safe_mode=1', this.httpOptions);
    }
}
