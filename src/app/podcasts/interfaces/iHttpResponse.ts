import { IHttpHeader } from './iHttpHeader';

export interface IHttpResponse {
    status: number;
    data: any;
    headers?: Array<IHttpHeader>;
    hasNewData?: boolean;
}