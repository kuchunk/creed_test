export interface IPodcasts {
    podcasts?: Array<IPodcast>;
}

export interface IPodcast {
    title: string;
    publisher: string;
    thumbnail: string;
    total_episodes: number;
    description: string;
    itunes_id: number;
    rss: string;
    website: string;
}
