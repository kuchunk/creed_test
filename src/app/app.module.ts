import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PodcastsComponent } from './podcasts/components/podcasts.component';
import { PodcastsService } from './podcasts/services/podcasts.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { PodcastModel } from './podcasts/models/podcastModel';

@NgModule({
  declarations: [
    AppComponent,
    PodcastsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    PodcastsComponent,
    PodcastsService,
    HttpClientModule,
    PodcastModel
  ],
  bootstrap: [
    AppComponent,
    PodcastsComponent
  ]
})
export class AppModule { }
